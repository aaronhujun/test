package com.kaian.loan.web;

import com.google.common.collect.Maps;
import com.kaian.loan.data.service.LoanService;
import com.kaian.loan.handle.HandleParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by AARON on 2015/1/13.
 */
@RestController()
public class LoanServiceController {

    private static final Logger LOG = LoggerFactory.getLogger(LoanServiceController.class);

//    @Autowired
//    private LoanTaskService loanTaskService;

//    @Autowired
//    private EventManage eventManage;

    @Autowired
    private LoanService loanService;

    /*@RequestMapping("addTask")
    public void addTask(@RequestParam(value="name") String name,
                        @RequestParam(value="orgCode") String orgCode,
                        HandleParams hp) {
        LOG.debug("INTO addTask");
        loanTaskService.addTask(name,orgCode,hp);
        eventManage.executeEvent(name,orgCode,hp);
    }*/

    @RequestMapping(value = "sign",produces= "application/json;charset=UTF-8")
    public ResultInfo sign(@RequestParam(value="id") String id,
                           @RequestParam(value="userName") String userName,
                           @RequestParam(value="payMerchId") String payMerchId){
        HandleParams params = new HandleParams();
        params.setId(id);
        Map<String,Object> map = Maps.newHashMap();
        map.put("userName",userName);
        map.put("payMerchId",payMerchId);
        params.setMap(map);
        loanService.sign(params);
        LOG.info("SIGN END ----------------------------");
//        System.out.print("------------------------------------>>>>>>>>>");
        ResultInfo resultInfo = new ResultInfo("SUCCESS","操作成功");
        return resultInfo;
    }



}
