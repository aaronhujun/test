package com.kaian.loan.web;

import java.io.Serializable;

/**
 * Created by AARON on 2015/1/15.
 */
public class ResultInfo implements Serializable{

    public ResultInfo(String result,String memo) {
        this.memo = memo;
        this.result = result;
    }

    private String result;

    private String memo;

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
