package com.kaian.loan.web;

import com.kaian.loan.data.service.LoanService;
import com.kaian.loan.handle.HandleParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by AARON on 2015/1/19.
 */
@Controller
@RequestMapping("/views/*")
public class LoanMonitorController {

    private static final Logger LOG = LoggerFactory.getLogger(LoanMonitorController.class);

    @Autowired
    LoanService loanService;

    @RequestMapping(value = "findMonitorList")
    public String findAllList(Model model){
        model.addAttribute("list", loanService.findAllList(new HandleParams()));
        return "views/loanmonitor";
    }

}
