package com.kaian.loan.handle;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by AARON on 2015/1/13.
 */
public class HandleParams {

    private String id;

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    private Map<String,Object> map = Maps.newHashMap();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
