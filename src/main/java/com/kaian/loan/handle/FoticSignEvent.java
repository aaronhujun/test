package com.kaian.loan.handle;

import com.kaian.loan.data.service.FoticLoanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by AARON on 2015/1/13.
 */
@Component("foticSign")
public class FoticSignEvent implements LoanHandle {

    static final Logger LOG = LoggerFactory.getLogger(FoticSignEvent.class);

    @Autowired
    private FoticLoanService foticLoanService;

    @Override
    public void callEvent(String name, String orgCode, HandleParams hp) {

        LOG.debug("INTO foticSign event");
//        foticLoanService.sign();
    }
}
