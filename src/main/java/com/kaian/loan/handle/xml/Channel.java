package com.kaian.loan.handle.xml;

import java.util.List;

/**
 * Created by AARON on 2015/1/13.
 */
public class Channel {


    private String name;

    private List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
