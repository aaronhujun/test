package com.kaian.loan.handle.xml;

/**
 * Created by AARON on 2015/1/13.
 */
public class Event {



    private String name;
    private String describe;
    private String handle;

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
