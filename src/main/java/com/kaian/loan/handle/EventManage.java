package com.kaian.loan.handle;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.common.collect.Maps;
import com.kaian.loan.handle.xml.Channel;
import com.kaian.loan.handle.xml.Event;
import com.kaian.loan.handle.xml.Loanservices;
import com.kaian.loan.util.SpringContextUtil;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Created by AARON on 2015/1/13.
 */
@Component
public class EventManage {

    private Loanservices loanservices;

    private Map<String,Map<String,Object>> allChannelEvent;

    EventManage(){

        //init();
    }

    private void init()
    {
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper mapper = new XmlMapper(module);
        InputStream in = EventManage.class.getResourceAsStream("/spring/LoanService.xml");
        try {
            loanservices = mapper.readValue(in, Loanservices.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        Map<String,Map<String,Object>> map = Maps.newHashMap();
        for(Channel channel:loanservices.getChannel()){
            Map<String,Object> events = Maps.newHashMap();
            for(Event event:channel.getEvents()){
                events.put(event.getName(),event);
            }
            allChannelEvent.put(channel.getName(),events);
        }
    }

    public void executeEvent(String name,String orgCode,HandleParams hp){
        Event event = (Event) allChannelEvent.get(orgCode).get(name);
        if(event==null){
            throw new RuntimeException(orgCode+"中没有["+name+"]对应可执行事件");
        }
        LoanHandle handle = (LoanHandle) SpringContextUtil.getBean(event.getHandle());
        handle.callEvent(name,orgCode,hp);
    }

    public static void main(String[] args) {
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper mapper = new XmlMapper(module);
        try {
            InputStream in = EventManage.class.getResourceAsStream("/spring/LoanService.xml");
            System.out.println(in);
//            Loanservices value = mapper.readValue("<Loanservices><channel><name>fotic</name><event><describe>dddd</describe></event></channel></Loanservices>", Loanservices.class);
            Loanservices value = mapper.readValue(in, Loanservices.class);
            System.out.println(value.getChannel().get(0).getEvents().get(0).getDescribe());
            System.out.println(value.getChannel().get(0).getEvents().get(0).getName());
            System.out.println(value.getChannel().get(0).getEvents().get(0).getHandle());
            System.out.println(value);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
