package com.kaian.loan.data.service;

import com.kaian.loan.handle.HandleParams;

/**
 * Created by AARON on 2015/1/13.
 */
public interface LoanTaskService {

    int addTask(String name,String orgCode,HandleParams hp);
}
