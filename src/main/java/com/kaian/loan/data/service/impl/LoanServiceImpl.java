package com.kaian.loan.data.service.impl;

import com.kaian.loan.data.model.ContractInfo;
import com.kaian.loan.data.model.LoansInfo;
import com.kaian.loan.data.model.LoansInfoLog;
import com.kaian.loan.data.service.LoanService;
import com.kaian.loan.data.util.PageInfo;
import com.kaian.loan.handle.HandleParams;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by AARON on 2015/1/16.
 */
@Service("loanService")
@Transactional
public class LoanServiceImpl extends BaseService implements LoanService {

    @Override
    public void sign(HandleParams params) {
        String userName = (String)params.getMap().get("userName");
        String payMerchId = (String)params.getMap().get("payMerchId");
        ContractInfo contractInfo = get(ContractInfo.class, params.getId());

        LoansInfo loansInfo = get(LoansInfo.class, "L" + contractInfo.getApplyCode().substring(1));
        if(loansInfo ==null)
            loansInfo = new LoansInfo();
        else
            return;
        BeanUtils.copyProperties(contractInfo, loansInfo);
        loansInfo.setLoanCode("L" + contractInfo.getApplyCode().substring(1));
        loansInfo.setCreateTime(new Date());
        loansInfo.setOperator(userName);
        loansInfo.setStatus("0");
        loansInfo.setPayMerchId(payMerchId);
        update(loansInfo);
//        loansInfo.setContractNo(contractInfo.getContractNo());
//        loansInfo.setApplyCode(contractInfo.getApplyCode());
//        loansInfo.setProductCode(contractInfo.getProductCode());
//        loansInfo.setProductName(contractInfo.getProductName());
//        loansInfo.setLoanAmount(contractInfo.getLoanAmount());
//        loansInfo.setLoanLife(contractInfo.getLoanLife());
//        loansInfo.setInterest(contractInfo.getInterest());
//        loansInfo.setContractAmount(contractInfo.getContractAmount());
//        loansInfo.setName(contractInfo.getName());
//        loansInfo.setCardNo(contractInfo.getCardNo());
//        loansInfo.setLoanDay(contractInfo.getLoanDay());
//        loansInfo.setAcctNo(contractInfo.getAcctNo());
//        loansInfo.setBankCode(contractInfo.getBankCode());
//        loansInfo.setBank(contractInfo.getBank());
//        loansInfo.setBankAddr(contractInfo.getBankAddr());
//        loansInfo.setOrgCode(contractInfo.getOrgCode());
//        loansInfo.setThirdCode(contractInfo.getThirdCode());
//        loansInfo.setTrustPlan(contractInfo.getTrustPlan());
//        loansInfo.setBranch(contractInfo.getBranch());
//        loansInfo.setBranchCode(contractInfo.getBranchCode());
//        loansInfo.setServiceFee(contractInfo.getServiceFee());
        LoansInfoLog infoLog = new LoansInfoLog(loansInfo,"签订");
        update(infoLog);
    }

    @Override
    public List<LoansInfo> findAllList(HandleParams params) {
        PageInfo pageInfo = new PageInfo();
        pageInfo.setPage(1);
        pageInfo.setSize(20);
        List<LoansInfo> list = findList(LoansInfo.class,"", pageInfo);
        return list;
    }
}
