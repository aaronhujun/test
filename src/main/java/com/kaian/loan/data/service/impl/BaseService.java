package com.kaian.loan.data.service.impl;

import com.kaian.loan.data.util.PageInfo;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by AARON on 2015/1/14.
 */
abstract class BaseService {

    @Autowired
    private SessionFactory sessionFactory;

    protected<T extends Serializable> Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * It always hit the database and return the real object, an object that represent the database row, not proxy.
     * @param cls
     * @param id
     * @param <T>
     * @return
     */
    protected <T extends Serializable> T get(Class<T> cls, String id){
        return (T)getSession().get(cls, id);
    }

    /**
     * It will always return a “proxy” (Hibernate term) without hitting the database. In Hibernate,
     * proxy is an object with the given identifier value, its properties are not initialized yet, it just look like a temporary fake object.
     * @param cls
     * @param id
     * @param <T>
     * @return
     */
    protected <T extends Serializable> T load(Class<T> cls, String id){
        return (T) getSession().load(cls, id);
    }

    protected  <T extends Serializable> void save(T obj){

        getSession().save(obj);
    }

    protected <T extends Serializable> List<T> findList(Class<T> cls, String whe, PageInfo page,Object... pars) {
        Query query = getSession().createQuery(getHql(cls, null, whe));
        if(pars != null && pars.length != 0) {
            for (int i = 0; i < pars.length; i++) {
                query.setParameter(i, pars[i]);
            }
        }
        return query.setMaxResults(page.getSize()).setFirstResult(page.getFirstIndex()).list();
    }

    protected <T extends Serializable> void update(T obj) {

        getSession().saveOrUpdate(obj);
    }

    protected <T extends Serializable> List<T> findList(Class<T> cls, String whe, Object... pars) {
        Query query = getSession().createQuery(getHql(cls, null, whe));
        if(pars != null && pars.length != 0) {
            for (int i = 0; i < pars.length; i++) {
                query.setParameter(i, pars[i]);
            }
        }
        return query.list();
    }

    private <T extends Serializable> String getHql(Class<T> cls, String f, String whe) {
        String hql = "from " + cls.getName() + " a";
        if(!StringUtils.isEmpty(whe)) {
            hql += (" where " + whe);
        }
        return hql;
    }


}
