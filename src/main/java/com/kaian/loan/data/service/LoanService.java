package com.kaian.loan.data.service;

import com.kaian.loan.data.model.LoansInfo;
import com.kaian.loan.handle.HandleParams;

import java.util.List;

/**
 * Created by AARON on 2015/1/13.
 */
public interface LoanService {

    void sign(HandleParams params);

    List<LoansInfo> findAllList(HandleParams params);
}
