package com.kaian.loan.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by AARON on 2015/1/14.
 */
@Entity
@Table(name = "ACCT_LOANS_INFO")
public class LoansInfo implements Serializable {
    @Id
    private String loanCode;// 放款编号
    private String contractNo;// 合同号
    private String applyCode;// 申请编号

    private String productCode;// 产品代码
    private String productName;// 产品名称
    private BigDecimal loanAmount;// 放款金额
    private Integer loanLife;// 贷款期限
    private BigDecimal interest;// 利息
    private BigDecimal contractAmount;// 合同金额

    private String name;// 客户姓名（必须和银行账户开户姓名一致）
    private String cardNo;// 客户身份证号码

    private Date loanDay;// 放款时间 --新增 t+1 天

    private String acctNo;// 客户放款银行账号
    private String bankCode;// 放款银行代码
    private String bank;// 放款银行名称
    private String bankAddr;// 放款银行开户地址

    private String orgCode; // 合作金融机构
    private String thirdCode;// 第三方支付代码
    private String payMerchId;// 中腾信在该第三方支付中代付的商户代码

    private Date createTime;
    private Date updateTime;
    private String operator;
    private String branchCode;// 申请门店号
    private String branch;// 门店名


    /**
     * 信托： 0:待门店放款确认，1:门店放款确认，2:门店取消（合同取消），3:财务经办确认， 4:财务经办锁定，5:财务复核确认，6:放款成功，7:放款失败，8:放款中
     * 中行：20：已发送   	30：审批拒绝
     * 有利：20：已发送
     */
    private String status;

    private String bankAppStatus = "";

    //信托：资金调拨状态：0未调拨，1已调拨
    //P2P： 0未发标，1满标，2发标中
    private String fundStatus = "0";

    private String reason;//放款成功失败原因

    private String trustPlan;

    private String batchCode;

    private String deducteFlag = "0";// 扣款文件生产标志 0：未生成 1：生成

    private String sendflag = "0";// 发送账务系统标志 0：未发送 1：发送

    private String isExport = "0";// 是否导出

    private boolean needCancelFile;// 是否需要生成调减单
    private BigDecimal serviceFee;//服务费

    @Id
    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getApplyCode() {
        return applyCode;
    }

    public void setApplyCode(String applyCode) {
        this.applyCode = applyCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getLoanLife() {
        return loanLife;
    }

    public void setLoanLife(Integer loanLife) {
        this.loanLife = loanLife;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(BigDecimal contractAmount) {
        this.contractAmount = contractAmount;
    }

    public Date getLoanDay() {
        return loanDay;
    }

    public void setLoanDay(Date loanDay) {
        this.loanDay = loanDay;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAddr() {
        return bankAddr;
    }

    public void setBankAddr(String bankAddr) {
        this.bankAddr = bankAddr;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getThirdCode() {
        return thirdCode;
    }

    public void setThirdCode(String thirdCode) {
        this.thirdCode = thirdCode;
    }

    public String getPayMerchId() {
        return payMerchId;
    }

    public void setPayMerchId(String payMerchId) {
        this.payMerchId = payMerchId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getTrustPlan() {
        return trustPlan;
    }

    public void setTrustPlan(String trustPlan) {
        this.trustPlan = trustPlan;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFundStatus() {
        return fundStatus;
    }

    public void setFundStatus(String fundStatus) {
        this.fundStatus = fundStatus;
    }

    public String getBankAppStatus() {
        return bankAppStatus;
    }

    public void setBankAppStatus(String bankAppStatus) {
        this.bankAppStatus = bankAppStatus;
    }

    public String getDeducteFlag() {
        return deducteFlag;
    }

    public void setDeducteFlag(String deducteFlag) {
        this.deducteFlag = deducteFlag;
    }

    public String getSendflag() {
        return sendflag;
    }

    public void setSendflag(String sendflag) {
        this.sendflag = sendflag;
    }

    public String getIsExport() {
        return isExport;
    }

    public void setIsExport(String isExport) {
        this.isExport = isExport;
    }

    @Column(name = "NEED_CANCEL_FILE_", columnDefinition = "NUMBER(1) DEFAULT 0")
    public boolean getNeedCancelFile() {
        return needCancelFile;
    }

    public void setNeedCancelFile(boolean needCancelFile) {
        this.needCancelFile = needCancelFile;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

}

