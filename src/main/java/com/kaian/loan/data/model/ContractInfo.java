package com.kaian.loan.data.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by AARON on 2015/1/14.
 */
@Entity
@Table(name = "CONTRACT_INFO")
public class ContractInfo implements Serializable{

    @Id
    @GeneratedValue(generator = "idGenerator")
    @GenericGenerator(name = "idGenerator", strategy = "assigned")
    private String contractNo;//合同编号
    private String accountNo;//系统账户号
    private String custNo;//客户编号
    private String applyCode;//申请编号
    private String productCode;// 产品代码
    private String productName;//产品名称
    private BigDecimal applyAmount;//申请金额
    private BigDecimal loanAmount;//放款金额
    private Integer loanLife;//贷款期限
    private BigDecimal interest;//利息
    @Column(precision = 10, scale = 6)
    private BigDecimal inteRate;//利息月利率
    @Column(precision = 10, scale = 6)
    private BigDecimal yearInteRate;//年利率
    @Column(precision = 10, scale = 6)
    private BigDecimal punishmentRate;//罚息率  punishmentRate
    private BigDecimal serviceFee;//服务费
    @Column(precision = 10, scale = 6)
    private BigDecimal serviceRate;//服务费率
    @Column(precision = 10, scale = 6)
    private BigDecimal penaltyRate;//滞纳金率  penaltyRate
    @Column(precision = 10, scale = 6)
    private BigDecimal prepaymentRate;//提前还款费率
    @Column(precision = 10, scale = 6)
    private BigDecimal conAmountRate;//到手金额/合同金额
    private Integer loanPurpose;// 贷款用途
    private String loanType;//贷款类型

    private String payType;//本金还款方式：等额本息，等额本金，目前只支持等额本息
    private String sPayType;//服务费付款方式//趸交；期缴；----目前只支持趸交

    private String name;//客户姓名（必须和银行账户开户姓名一致）
    private String cardNo;//客户身份证号码

    private String acctNo;//客户放款银行账号
    private String bankCode;//放款银行代码
    private String bank;//放款银行名称
    private String bankAddr;//放款银行开户地址
    private String bankAppDate;//银行审批日期

    private String reAcctNo;//客户扣款银行账号
    private String reBankCode;//扣款银行代码
    private String reBank;//扣款银行名称
    private String reBankAddr;//扣款银行开户地址

    private String ztxAcctNo;//中腾信归集账户号
    private String ztxAcctName;//中腾信归集账户户名
    private String ztxBankCode;//中腾信归集账户银行代码
    private String ztxBank;//中腾信归集账户银行名称

    private String fundAcctNo;//资金方账户号
    private String fundAcctName;//资金方账户户名
    private String fundBankCode;//资金方账户银行代码
    private String fundBank;//资金方账户银行名称

    private String startDate;//合同起始日期
    private String deadLine;//合同截至日期
    private String signerCode;//合同签订员代码
    private String signer;//合同签订员姓名
    private Date signDate;//合同签订日期

    private String branchCode;//申请门店号
    private String branch;//申请门店名称
    private String respSalesCode;// 销售人员代码
    private String respSales;// 销售人员名称
    private String teamManager;// 团队经理

    private String orgCode; // 合作金融机构
    private String trustPlan;//信托计划
    private String orgBranchCode; // 合作金融机构放款网点
    private String thirdCode;//第三方支付代码

    private BigDecimal monthPay;//每月还款本息
    private BigDecimal contractAmount;//合同金额
    //add zhanghua
    private Date  loanDay;//放款时间  --新增 t+1 天
    private String status;//合同状态 0：未签订  1： 签订 2：已建账3:取消


    //为签合同打印 时用(暂无用)
    private String startYear;//合同起始日期年
    private String startMonth;//合同起始日期月
    private String startDay;//合同起始日期日
    private String deadYear;//合同截至日期年
    private String deadMonth;//合同截至日期月
    private String deadDay;//合同截至日期日
    private Integer signYear;//合同签订日期年
    private Integer signMonth;//合同签订日期月
    private Integer signDay;//合同签订日期日

    private String channel;//进件渠道


    private String updater;
    private Date updateTime;

    private BigDecimal yooliSfRate;//yooli手续费

    @Column(name="INTEREST_RESET_TYPE")
    private String interestResetType;// 利息重置标志
    @Column(name="REPAYMENT_CALC_TYPE")
    private String repaymentType;// 还款方式
    @Column(name="SERVICEFEE_SETTLEMENT_TYPE")
    private BigDecimal singleServiceFeeRate;// 趸缴服务费比例

    @Column(columnDefinition = "VARCHAR2(255 BYTE) DEFAULT '0'")
    public String getChannel() {
        return channel;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public Date getLoanDay() {
        return loanDay;
    }
    public void setLoanDay(Date loanDay) {
        this.loanDay = loanDay;
    }
    public String getContractNo() {
        return contractNo;
    }
    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }
    public String getAccountNo() {
        return accountNo;
    }
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    public String getApplyCode() {
        return applyCode;
    }
    public void setApplyCode(String applyCode) {
        this.applyCode = applyCode;
    }
    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public BigDecimal getApplyAmount() {
        return applyAmount;
    }
    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }
    public BigDecimal getLoanAmount() {
        return loanAmount;
    }
    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }
    public BigDecimal getInterest() {
        return interest;
    }
    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }
    public BigDecimal getPenaltyRate() {
        return penaltyRate;
    }
    public void setPenaltyRate(BigDecimal penaltyRate) {
        this.penaltyRate = penaltyRate;
    }
    public BigDecimal getServiceFee() {
        return serviceFee;
    }
    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }
    public BigDecimal getPunishmentRate() {
        return punishmentRate;
    }
    public void setPunishmentRate(BigDecimal punishmentRate) {
        this.punishmentRate = punishmentRate;
    }
    public Integer getLoanPurpose() {
        return loanPurpose;
    }
    public void setLoanPurpose(Integer loanPurpose) {
        this.loanPurpose = loanPurpose;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCardNo() {
        return cardNo;
    }
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
    public String getAcctNo() {
        return acctNo;
    }
    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getDeadLine() {
        return deadLine;
    }
    public void setDeadLine(String deadLine) {
        this.deadLine = deadLine;
    }
    public String getSignerCode() {
        return signerCode;
    }
    public void setSignerCode(String signerCode) {
        this.signerCode = signerCode;
    }
    public String getSigner() {
        return signer;
    }
    public void setSigner(String signer) {
        this.signer = signer;
    }
    public Date getSignDate() {
        return signDate;
    }
    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }
    public String getBankCode() {
        return bankCode;
    }
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
    public String getBank() {
        return bank;
    }
    public void setBank(String bank) {
        this.bank = bank;
    }
    public String getBankAppDate() {
        return bankAppDate;
    }
    public void setBankAppDate(String bankAppDate) {
        this.bankAppDate = bankAppDate;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public String getBranch() {
        return branch;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public String getRespSalesCode() {
        return respSalesCode;
    }
    public void setRespSalesCode(String respSalesCode) {
        this.respSalesCode = respSalesCode;
    }
    public String getRespSales() {
        return respSales;
    }
    public void setRespSales(String respSales) {
        this.respSales = respSales;
    }
    public String getTeamManager() {
        return teamManager;
    }
    public void setTeamManager(String teamManager) {
        this.teamManager = teamManager;
    }
    public String getStartYear() {
        return startYear;
    }
    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }
    public String getStartMonth() {
        return startMonth;
    }
    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }
    public String getDeadYear() {
        return deadYear;
    }
    public void setDeadYear(String deadYear) {
        this.deadYear = deadYear;
    }
    public String getDeadMonth() {
        return deadMonth;
    }
    public void setDeadMonth(String deadMonth) {
        this.deadMonth = deadMonth;
    }
    public Integer getLoanLife() {
        return loanLife;
    }
    public void setLoanLife(Integer loanLife) {
        this.loanLife = loanLife;
    }
    public BigDecimal getInteRate() {
        return inteRate;
    }
    public void setInteRate(BigDecimal inteRate) {
        this.inteRate = inteRate;
    }
    public BigDecimal getServiceRate() {
        return serviceRate;
    }
    public void setServiceRate(BigDecimal serviceRate) {
        this.serviceRate = serviceRate;
    }
    public BigDecimal getPrepaymentRate() {
        return prepaymentRate;
    }
    public void setPrepaymentRate(BigDecimal prepaymentRate) {
        this.prepaymentRate = prepaymentRate;
    }
    public String getPayType() {
        return payType;
    }
    public void setPayType(String payType) {
        this.payType = payType;
    }
    public String getsPayType() {
        return sPayType;
    }
    public void setsPayType(String sPayType) {
        this.sPayType = sPayType;
    }
    public String getReAcctNo() {
        return reAcctNo;
    }
    public void setReAcctNo(String reAcctNo) {
        this.reAcctNo = reAcctNo;
    }
    public String getReBankCode() {
        return reBankCode;
    }
    public void setReBankCode(String reBankCode) {
        this.reBankCode = reBankCode;
    }
    public String getReBank() {
        return reBank;
    }
    public void setReBank(String reBank) {
        this.reBank = reBank;
    }
    public Integer getSignYear() {
        return signYear;
    }
    public void setSignYear(Integer signYear) {
        this.signYear = signYear;
    }
    public Integer getSignMonth() {
        return signMonth;
    }
    public void setSignMonth(Integer signMonth) {
        this.signMonth = signMonth;
    }
    public Integer getSignDay() {
        return signDay;
    }
    public void setSignDay(Integer signDay) {
        this.signDay = signDay;
    }
    public String getStartDay() {
        return startDay;
    }
    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }
    public String getDeadDay() {
        return deadDay;
    }
    public void setDeadDay(String deadDay) {
        this.deadDay = deadDay;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getLoanType() {
        return loanType;
    }
    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }
    public String getCustNo() {
        return custNo;
    }
    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }
    public String getZtxAcctNo() {
        return ztxAcctNo;
    }
    public void setZtxAcctNo(String ztxAcctNo) {
        this.ztxAcctNo = ztxAcctNo;
    }
    public String getZtxBankCode() {
        return ztxBankCode;
    }
    public void setZtxBankCode(String ztxBankCode) {
        this.ztxBankCode = ztxBankCode;
    }
    public String getZtxBank() {
        return ztxBank;
    }
    public void setZtxBank(String ztxBank) {
        this.ztxBank = ztxBank;
    }
    public String getFundAcctNo() {
        return fundAcctNo;
    }
    public void setFundAcctNo(String fundAcctNo) {
        this.fundAcctNo = fundAcctNo;
    }
    public String getFundBankCode() {
        return fundBankCode;
    }
    public void setFundBankCode(String fundBankCode) {
        this.fundBankCode = fundBankCode;
    }
    public String getFundBank() {
        return fundBank;
    }
    public void setFundBank(String fundBank) {
        this.fundBank = fundBank;
    }
    public String getOrgCode() {
        return orgCode;
    }
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
    public String getOrgBranchCode() {
        return orgBranchCode;
    }
    public void setOrgBranchCode(String orgBranchCode) {
        this.orgBranchCode = orgBranchCode;
    }
    public String getZtxAcctName() {
        return ztxAcctName;
    }
    public void setZtxAcctName(String ztxAcctName) {
        this.ztxAcctName = ztxAcctName;
    }
    public String getFundAcctName() {
        return fundAcctName;
    }
    public void setFundAcctName(String fundAcctName) {
        this.fundAcctName = fundAcctName;
    }
    public BigDecimal getContractAmount() {
        return contractAmount;
    }
    public void setContractAmount(BigDecimal contractAmount) {
        this.contractAmount = contractAmount;
    }
    public BigDecimal getMonthPay() {
        return monthPay;
    }
    public void setMonthPay(BigDecimal monthPay) {
        this.monthPay = monthPay;
    }
    public BigDecimal getConAmountRate() {
        return conAmountRate;
    }
    public void setConAmountRate(BigDecimal conAmountRate) {
        this.conAmountRate = conAmountRate;
    }
    public BigDecimal getYearInteRate() {
        return yearInteRate;
    }
    public void setYearInteRate(BigDecimal yearInteRate) {
        this.yearInteRate = yearInteRate;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getBankAddr() {
        return bankAddr;
    }
    public void setBankAddr(String bankAddr) {
        this.bankAddr = bankAddr;
    }
    public String getReBankAddr() {
        return reBankAddr;
    }
    public void setReBankAddr(String reBankAddr) {
        this.reBankAddr = reBankAddr;
    }
    public String getThirdCode() {
        return thirdCode;
    }
    public void setThirdCode(String thirdCode) {
        this.thirdCode = thirdCode;
    }
    public String getTrustPlan() {
        return trustPlan;
    }
    public void setTrustPlan(String trustPlan) {
        this.trustPlan = trustPlan;
    }
    public BigDecimal getYooliSfRate() {
        return yooliSfRate;
    }
    public void setYooliSfRate(BigDecimal yooliSfRate) {
        this.yooliSfRate = yooliSfRate;
    }
    public String getUpdater() {
        return updater;
    }
    public void setUpdater(String updater) {
        this.updater = updater;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getInterestResetType() {
        return interestResetType;
    }
    public void setInterestResetType(String interestResetType) {
        this.interestResetType = interestResetType;
    }
    public String getRepaymentType() {
        return repaymentType;
    }
    public void setRepaymentType(String repaymentType) {
        this.repaymentType = repaymentType;
    }
    public BigDecimal getSingleServiceFeeRate() {
        return singleServiceFeeRate;
    }
    public void setSingleServiceFeeRate(BigDecimal singleServiceFeeRate) {
        this.singleServiceFeeRate = singleServiceFeeRate;
    }
}