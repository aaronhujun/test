package com.kaian.loan.data.model;

import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by AARON on 2015/1/15.
 */
@Entity
@Table(name = "ACCT_LOANS_INFO_LOG")
public class LoansInfoLog implements Serializable {

    @Id
    @SequenceGenerator(name="M_ACCT_LOANS_INFO_LOG_GENERATOR",allocationSize=1, sequenceName="SEQ_ACCT_LOANS_INFO_LOG_ID")
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="M_ACCT_LOANS_INFO_LOG_GENERATOR")
    private long id;

    private String loanCode;// 放款编号


    private Date createTime;
    private String operator;

    /**
     * 信托： 0:待门店放款确认，1:门店放款确认，2:门店取消（合同取消），3:财务经办确认， 4:财务经办锁定，5:财务复核确认，6:放款成功，7:放款失败，8:放款中
     * 中行：20：已发送   	30：审批拒绝
     * 有利：20：已发送
     */
    private String status;


    private String reason;//放款成功失败原因

    private String orgCode; // 合作金融机构

    private String memo;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLoanCode() {
        return loanCode;
    }

    public void setLoanCode(String loanCode) {
        this.loanCode = loanCode;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LoansInfoLog(LoansInfo loansInfo,String memo) {
        BeanUtils.copyProperties(loansInfo,this);
        this.memo = memo;
    }
}
