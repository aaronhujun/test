package com.kaian.loan.data.util;

/**
 * Created by AARON on 2015/1/14.
 */
public class PageInfo {

    private int size;

    private int page;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getFirstIndex(){
        if(page==1)
            return 1;
        return (page-1)*size;
    }
}
